"use strict";

process.title = 'node-chat';

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 80

var webSocketsServerPort = 8080;  //Hardcoded on openshift

// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');

/**
 * Global variables
 */
var clients = [ ];

var dateFormat= require('dateformat');

/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
  // Not important for us. We're writing WebSocket server,
  // not HTTP server
});
server.listen(webSocketsServerPort, function() {
  console.log( dateFormat(new Date(), "UTC:hh:MM:ss") + ": Server is listening on port "
      + webSocketsServerPort);


});

/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
  httpServer: server, path:"/chat"
});

// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function(request) {
  console.log(dateFormat(new Date(), "UTC:hh:MM:ss") + ': Connection from origin '
      + request.origin + '.');

  // accept connection - you should check 'request.origin' to
  // make sure that client is connecting from your website
  // (http://en.wikipedia.org/wiki/Same_origin_policy)
  var connection = request.accept(null, request.origin); 
  // we need to know client index to remove them on 'close' event
  var index = clients.push(connection) - 1;

  console.log(dateFormat(new Date(), "UTC:hh:MM:ss") + ': Connection accepted.',index);


// client sent some message
connection.on('message', function(message) {
  if (message.type==='utf8') { // accept only text
    var m = JSON.parse(message.utf8Data);
    console.log(dateFormat(new Date(), "UTC:hh:MM:ss") + ": "+ message.utf8Data);
        
    // broadcast message to all connected clients
    var json = JSON.stringify({ tag: m.tag, message:m.message });
    for (var i=0;i<clients.length;i++) {
      //try { 
      console.log(i);
      if (clients[i]!==undefined) {
        clients[i].sendUTF(json); console.log(i);
      }
     // } catch(e) {
     //   console.dir(e);
     // }
    }
  }
});

  // client disconnected
  connection.on('close', function(connection) {
      console.log(dateFormat(new Date(), "UTC:hh:MM:ss") + ": Peer disconnected.");

      // remove client from the list of connected clients
      console.log(index,clients.length);
//      clients.splice(index, 1);
      clients[index] = undefined;
      console.log(index,clients.length);
  });
});
