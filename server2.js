"use strict";

process.title = 'node-chat-2018';

//var server_port = process.env.OPENSHIFT_NODEJS_PORT || 7777
var server_port = 8080


var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);

var path = require('path');

app.use(express.static('files'));
 
app.use(function (req, res, next) {
  console.log('middleware'); //console.dir(req);
  req.testing = 'testing';
  return next();
});
 
app.get('/', function(req, res, next){
  console.log('get route', req.testing);
  console.log(path.join(__dirname + '/index.html'));
  res.sendFile(path.join(__dirname + '/index.html'));
//  res.end();
});

var clients = [];
 
app.ws('/', function(ws, req) {     console.log('ws request!!!!');
  //var index = clients.push(ws)-1;
  clients.push(ws)-1;
  //console.log(index);

//  console.log(dateFormat(new Date(), "UTC:hh:MM:ss") + ': Connection accepted: ' + index);

  ws.on('message', function(msg) {
    console.log(msg);
    for (var i=0;i<clients.length;i++) {
      if (clients[i]!==undefined && clients[i].readyState==1) {
        clients[i].send(msg);
      }
    }
  });
  ws.on('close', function() {
    console.log('Closing ws');
    //clients[ws.index] = undefined;
    //console.log(ws.index,clients.length);
  });
  console.log('socket', req.testing);
});
 
app.listen(server_port);
console.log('Listening on',server_port);
